/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter14;

import javax.xml.ws.WebServiceRef;

/**
 *
 * @author efrem
 */
public class WebServiceConsumer {

    @WebServiceRef
    private static CardValidatorService cardValidatorService;

    public static void main(String[] args) {

        CreditCard creditCard = new CreditCard();
        creditCard.setNumber("12341234");
        creditCard.setExpiryDate("10/12");
        creditCard.setType("VISA");
        creditCard.setControlNumber(1234);

        Validator cardValidator = cardValidatorService.getCardValidatorPort();
        System.out.println(cardValidator.validate(creditCard));
    }
}

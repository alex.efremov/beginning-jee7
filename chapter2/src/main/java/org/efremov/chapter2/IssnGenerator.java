/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter2;

import javax.inject.Inject;
import java.util.Random;
import java.util.logging.Logger;

/**
 *
 * @author efrem
 */
@EightDigits
public class IssnGenerator implements NumberGenerator {
    // Attributes

    @Inject
    private Logger logger;

    // Business methods
    @Loggable
    public String generateNumber() {
        String issn = "8-" + Math.abs(new Random().nextInt());
        logger.info("Generated ISSN : " + issn);
        return issn;
    }
}

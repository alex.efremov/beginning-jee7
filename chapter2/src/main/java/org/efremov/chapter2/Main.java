/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter2;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;

/**
 *
 * @author efrem
 */
public class Main {
    public static void main(String[] args) {
        
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        
        BookService bookService = container.instance().select(BookService.class)
                .get();
        Book book = bookService.createBook("H2G2", 12.5f, "Geeky scifi Book");
        
        System.out.println(book);
        
        weld.shutdown();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter2;

/**
 *
 * @author efrem
 */
public interface NumberGenerator {
    // Business methods
    
    String generateNumber();
}

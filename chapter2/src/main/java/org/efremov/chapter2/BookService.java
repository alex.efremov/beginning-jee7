/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter2;

import javax.inject.Inject;

/**
 *
 * @author efrem
 */
@Loggable
public class BookService {
    // Attributes
    
    @Inject
    @ThirteenDigits
    private NumberGenerator numberGenerator;
    
    // Business methods
    
    public Book createBook(String title, Float price, String description) {
        Book book = new Book(title, price, description);
        book.setNumber(numberGenerator.generateNumber());
        return book;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter2;

import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 *
 * @author efrem
 */
public class BookServiceIT {
    // Methods
    
    @Test
    public void shouldCheckNumberIsMock() {
        Weld weld = new Weld();
        WeldContainer container = weld.initialize();
        
        BookService bookService = container.instance().select(BookService.class).get();
        
        Book book = bookService.createBook("H2G2", 12.5f, "Geeky scifi Book");
        
        assertTrue(book.getNumber().startsWith("MOCK"));
        
        weld.shutdown();
    }
}

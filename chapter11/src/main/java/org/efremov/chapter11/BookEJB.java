/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter11;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 *
 * @author efrem
 */
@Named
@Stateless
public class BookEJB {

    // ======================================
    // =             Attributes             =
    // ======================================
    @Inject
    private EntityManager em;

    // ======================================
    // =          Business methods          =
    // ======================================
    public Book createBook(Book book) {
        em.persist(book);
        return book;
    }

    public List<Book> findAllBooks() {
        return em.createNamedQuery("findAllBooks", Book.class).getResultList();
    }

    public Book findBookById(Long id) {
        return em.find(Book.class, id);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter11;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author efrem
 */
public class DBProducer {

    @Produces
    @PersistenceContext(unitName = "chapter11PU")
    private EntityManager em;
}

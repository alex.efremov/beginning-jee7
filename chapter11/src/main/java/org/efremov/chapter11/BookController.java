/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter11;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author efrem
 */
@Named
@RequestScoped
public class BookController {

    // ======================================
    // =             Attributes             =
    // ======================================
    @Inject
    private BookEJB bookEJB;
    private Book book = new Book();

    // ======================================
    // =           Public Methods           =
    // ======================================
    public String doCreateBook() {
        bookEJB.createBook(book);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Book created",
                "The book" + book.getTitle() + " has been created with id=" + book.getId()));
        return "newBook.xhtml";
    }

    public String doFindBookById() {
        book = bookEJB.findBookById(book.getId());
        if (book == null) 
            return "newBook.xhtml";
        return null;
    }

    // ======================================
    // =          Getters & Setters         =
    // ======================================
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter3;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 *
 * @author efrem
 */
public class CustomerIT {
    // Attributes
    
    private static ValidatorFactory vf;
    private static Validator validator;
    
    // Lifecycle Methods
    
    @BeforeClass
    public static void init() {
        vf = Validation.buildDefaultValidatorFactory();
        validator = vf.getValidator();
    }
    
    @AfterClass
    public static void close() {
        vf.close();
    }
    
    // Methods
    
    @Test
    public void shouldRaiseNoConstraintViolation() {
        
        Customer customer = new Customer("John", "Smith", "jmsith@gmail.com");
        
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
        assertEquals(0, violations.size());
    }
    
    @Test
    public void shouldRaiseConstraintViolationCauseInvalidEmail() {
        
        Customer customer = new Customer("John", "Smith", "DummyEmail");
        
        Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
        assertEquals(1, violations.size());
        assertEquals("invalid email address", violations.iterator().next().getMessage());
        assertEquals("DummyEmail", violations.iterator().next().getInvalidValue());
        assertEquals("{org.efremov.chapter3.Email.message}", violations.iterator().next().getMessageTemplate());
    }
}

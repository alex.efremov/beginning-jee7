/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter13client;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Date;

/**
 *
 * @author efrem
 */
public class OrderProducer {
    // ======================================
    // =           Public Methods           =
    // ======================================

    public static void main(String[] args) throws NamingException {

//        if (args.length != 1) {
//            System.out.println("usage : enter an amount");
//            System.exit(0);
//        }

        int am = 2000;

        System.out.println("Sending message with amount = " + am);

        // Creates an orderDto with a total amount parameter
        Float totalAmount = Float.valueOf(am);
        OrderDTO order = new OrderDTO(1234l, new Date(), "Serge Gainsbourg", totalAmount);

        // Gets the JNDI context
        Context jndiContext = new InitialContext();

        // Looks up the administered objects
        ConnectionFactory connectionFactory = (ConnectionFactory) jndiContext.lookup("jms/javaee7/ConnectionFactory");
        Destination topic = (Destination) jndiContext.lookup("jms/javaee7/Topic");

        try (JMSContext jmsContext = connectionFactory.createContext()) {
            // Sends an object message to the topic
            jmsContext.createProducer().setProperty("orderAmount", totalAmount).send(topic, order);
            System.out.println("\nOrder sent : " + order.toString());
        }
    }
}

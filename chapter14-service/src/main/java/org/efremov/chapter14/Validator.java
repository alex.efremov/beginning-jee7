/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter14;

import javax.jws.WebService;

/**
 *
 * @author efrem
 */

@WebService
public interface Validator {

  public boolean validate(CreditCard creditCard);

}

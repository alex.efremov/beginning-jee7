/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.efremov.chapter14;

import javax.jws.WebService;

/**
 *
 * @author efrem
 */
@WebService(endpointInterface = "org.efremov.chapter14.Validator")
public class CardValidator implements Validator {

  // ======================================
  // =           Public Methods           =
  // ======================================

  public boolean validate(CreditCard creditCard) {

    Character lastDigit = creditCard.getNumber().charAt(creditCard.getNumber().length() - 1);

    if (Integer.parseInt(lastDigit.toString()) % 2 == 0) {
      return true;
    } else {
      return false;
    }
  }
}

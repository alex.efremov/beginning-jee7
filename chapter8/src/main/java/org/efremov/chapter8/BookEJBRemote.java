package org.efremov.chapter8;

import javax.ejb.Remote;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 
 * @author efrem
 *
 */
@Remote
public interface BookEJBRemote {

	// Public Methods

	List<Book> findBooks();

	@NotNull
	Book createBook(@NotNull Book book);

	void deleteBook(@NotNull Book book);

	@NotNull
	Book updateBook(@NotNull Book book);
}

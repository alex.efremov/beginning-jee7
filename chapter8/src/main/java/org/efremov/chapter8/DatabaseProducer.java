package org.efremov.chapter8;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 
 * @author efrem
 *
 */
public class DatabaseProducer {

	// Attributes

	@Produces
	@PersistenceContext(unitName = "chapter08PU")
	private EntityManager em;
}
